#!/bin/zsh
set -e

# Make a new sequentially numbered file based on the template
DECISIONS_DIR="decisions"

drs=($DECISIONS_DIR/DR-*.md)
last=${drs[@][0-1]}
num=${last//[!0-9]/}
next_num=$(expr $num + 5)
export DR_NUMBER=$(printf "DR-%04d" $next_num)
next_file=$DECISIONS_DIR/$DR_NUMBER.md
echo Creating "$next_file"
cat ./etc/template/DR_template.md | CREATION_DATE=$(date +"%Y-%m-%d")  envsubst > "$next_file"
git add "$next_file"
