# Architectural Decision Records

This is a log of '[architecturally significant](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions)' decisions that concern Crossref's APIs and other systems.

You can [browse the decisions](https://crossref.gitlab.io/architecture-decision-records/). 

## Contributing

Check out this repo. To add a new numbered record run `./new.sh`. 

Note that the `envsubst` command used by `new.sh` is not available by default in macOS. You can get it by installing the `gettext` package, e.g. `brew install gettext`.

[Obsidian](https://obsidian.md/) is a nice editor for authoring and browsing.  

After you add a record, rebuild the [index](decisions/index.md) run `./index.sh`.

See [META.md](./etc/META.md) for more info.

## License

The content of this site is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/) 