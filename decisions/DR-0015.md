---
tags:
- testing
- development
---
# DR-0015: Automatic testing for new features
* Status: accepted
* Deciders: jwass, japaro, gbilder
* Date: 2019-06-24

## Context and Problem Statement

As of [March 2019](https://gitlab.com/crossref/content_system/-/tree/4b402ba46c8e4b272fd69f98b39d43e9c7cb6076) there are no unit tests in the [Content System](https://gitlab.com/crossref/content_system) codebase. As of [February 2018](https://gitlab.com/crossref/rest_api/-/commits/2fdfe98e0d3eb14ce0697dd7ba9b283b622e6198/test/cayenne) there were almost none in the [Cayenne REST API](https://gitlab.com/crossref/rest_api) codebase. Event Data has tests but they weren't enforced as part of the build pipeline.

Both the Content System and Cayenne REST API codebases were built by gradual iteration and manual testing. This makes development a slow and error-prone process. 

It also puts a damper on new feature development which does not suit us at this point.

## Decision Drivers 
* On-board new developers.
* Not rely on institutional knowledge.
* Be able to deprecate and remove functionality without worrying about unintended consequences.
* Eventually welcoming open source collaborators.

## Considered Options
* Prior: Manual testing every feature.

## Decision Outcome
- All new functionality, where possible to be unit or integration tested.
- Tests run in Continuous Integration builds to enforce that they always run.
- All files needed to run tests included in source code repositories.

### Positive Consequences 
* It's easier to regression-proof changes to our system.
* Specifications are executable.

### Negative Consequences 
* We need to take a bit more care in writing specifications in order for them to be automatically testable.

## Links 
* [First test commit to CS](https://gitlab.com/crossref/content_system/-/commit/b9c4bdaf9510bb1c138f48b44a8d1b350be6116f)
- [Tech Update 2](https://docs.google.com/document/d/1vQj7Uih1EdzZylPZ-HLV3U_xV5xxdrQmxRb3de9_V7c/edit)
