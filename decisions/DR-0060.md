---
tags:
  - development
---
# DR-0060: SONAR Test coverage quality gate
* Status: accepted
* Deciders: japaro, jwass
* Date: 2020-05-19

## Context and Problem Statement
TODO

## Decision Drivers 
TODO

## Considered Options
TODO

## Decision Outcome
- SONAR is used in new projects.
- SONAR quality gate is included in CI files for automatic checking as part of build.
- Builds that fail the SONAR quality gate are treated as broken.
- SONAR Cloud subscription.

### Positive Consequences 
* We catch more bugs.
* Code review has an objective baseline.

### Negative Consequences 
* Builds might break if failing code is checked in.
* It's hard to back-port to legacy software.
* Doesn't apply to every language, so we may need a different approach for Clojure and JavaScript / TypeScript.

## Links 
* [SONAR Cloud](https://sonarcloud.io/projects)