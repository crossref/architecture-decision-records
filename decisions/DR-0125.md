---
tags:
  - item-graph
  - design
---
# DR-0125: Item Graph data model covers entire Research Nexus

* Status: accepted
* Deciders: jwass, gbilder, 
* Authors: jwass
* Date: 2021-09-01

## Context and Problem Statement

This is a very broad decision, and this document may appear quite vague. It does, however, mark a turning point in our system design, so deserves its own decision record. Many of the subsequent decision records (tagged 'item-graph') get into detail, and are predicated on this one. 

We have several data models, some overlapping, that cover our own systems and those of our community. For an illustrative example see [[DR-0275]]. The Research Nexus offers a compelling new way of looking at data modelling. TODO

## Decision Drivers 

Enable desired functionality that's predicated on the research nexus:

* Enable data about the same metadata from multiple asserting parties, recording provenance
* Embrace the fact that much of our metadata records relationships with external systems.
* Create a model that can be extended to other similar Persistent Identifier systems without extra code.

Broaden access to the data model:

* Surface the data model to public schemas, not purely in code.
* Create a reusable model that can be translated to, and from, relevant data formats from our community.

Enable participation

* Create a reusable codebase and data model that doesn't assume that Crossref at the centre. Doing so is a true test of how well we implement the Research Nexus, as it allows users to view data from perspectives other than our own.

## Considered Options

1. Existing Content System. This models the entities in the XML as SQL tables in a fairly conventional manner. It also models external connections (e.g. ORCID links) with specific tables. The Relationships table models relationships between Crossref-registered content differently to relationships to other research objects in the community.
2. Existing Cayenne REST API. This represents individual metadata records, and denormalised views of some of Content System's entity model. It is therefore effective at modelling in a closer manner to the Research Nexus concept. However it has no first-class concept of provenance, and isn't enough.
3. Research Nexus Item Graph. Record Items, Relationships, Properties and other pieces of metadata in a graph structure, with all statements recorded along with provenance. 

## Decision Outcome

3 — Research Nexus Item Graph

## Links

* [[DR-0115]]
* [Technology update 9](https://docs.google.com/document/d/1tvp0r8NE2ALADxq8wksn2AN88OSmEhoOgSz6TWficEs/edit#heading=h.7j23uy3jmn7m)
* [Technology update 9.5](https://docs.google.com/document/d/16pe2jcReOUr03whzk-4AIBAa2nN9exoQWb3sbXF3NZI/edit#heading=h.utmc4t6b89vr)
* [Research Nexus / content type evolution strategy](https://docs.google.com/document/d/1hOMTWm544rNHajCi7gX5TJC9a9mIGzzaCmh0I5koDAY/edit#heading=h.e16wuveso5uo)