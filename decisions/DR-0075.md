---
tags:
- rest-api
- infrastructure
---
# DR-0075: Deploy images to ECR per region per pool / environment
* Status: accepted
* Deciders: jschuweiler
* Date: 2020-10-05

## Context and Problem Statement

Resulting from [[DR-0070]], resources are entirely separate between environments. This means that not only the are the Elastic Search clusters replicated, but so is all the supporting machinery. The combinatorics of pool × role × environment can add up.

## Considered Options
 - Modular, templated, replicated
 - Less modular, less replication

## Decision Outcome

 - Docker images are built once but uploaded to 48 separate Docker ECR repositories.

As of 2022 (a while after the decision was made), the set of environments was:

- internal-api-research, 
- plus-api-research, 
- polite-api-research, 
- public-api-research, 
- internal-indexer-research, 
- plus-indexer-research, 
- polite-indexer-research, 
- public-indexer-research, 
- internal-scheduled-tasks-research, 
- plus-scheduled-tasks-research, 
- polite-scheduled-tasks-research, 
- public-scheduled-tasks-research, 
- api-scanner-internal-research, 
- api-scanner-plus-research, 
- api-scanner-polite-research, 
- api-scanner-public-research, 
- internal-api-staging,
- plus-api-staging,
- polite-api-staging,
- public-api-staging,
- internal-indexer-staging,
- plus-indexer-staging,
- polite-indexer-staging,
- public-indexer-staging,
- internal-scheduled-tasks-staging,
- plus-scheduled-tasks-staging,
- polite-scheduled-tasks-staging,
- public-scheduled-tasks-staging,
- api-scanner-internal-staging,
- api-scanner-plus-staging,
- api-scanner-polite-staging,
- api-scanner-public-staging
- internal-api-production, 
- plus-api-production, 
- polite-api-production, 
- public-api-production, 
- internal-indexer-production, 
- plus-indexer-production, 
- polite-indexer-production, 
- public-indexer-production, 
- internal-scheduled-tasks-production, 
- plus-scheduled-tasks-production, 
- polite-scheduled-tasks-production, 
- public-scheduled-tasks-production, 
- api-scanner-internal-production, 
- api-scanner-plus-production, 
- api-scanner-polite-production,
- api-scanner-public-production

### Positive Consequences 

 - More uniform across pools

### Negative Consequences 
 
- More resources to keep an eye on, maintain
- Larger surface area to debug

## Links 

* <https://gitlab.com/crossref/rest_api/-/commit/8e7639035271870a370f03c03b80b656210d97c8>
