---
tags:
- manifold
- sql
- database
---
# DR-0348: 'pk' vs 'id' in Manifold SQL Schema
* Status: accepted
* Deciders: jwass
* Date: 2022-06-25

## Context and Problem Statement
Manifold provides the abstraction of an Item Graph, in which Items can be linked via Identifiers (e.g. DOIs, ORCID IDs, URLs etc).

The implementation is done in PostgreSQL. Every table (item, item identifier, relationship type, assertion etc) has a primary key column. By convention, in the wild, this is usually labelled 'id'. But 'id', being short for 'identity', can be confused for Item Identifier.

Because the concept of identity is present, and means much the same thing, at both levels of abstraction, it could be confusing.

## Decision Outcome
Use 'identifier' in the Item Graph abstraction. In the code and database schema use the field name `pk` (Primary Key) instead of `id`.