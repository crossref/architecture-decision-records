---
tags:
- metadata
- policy
- posi
---
# DR-0265: All references open

* Status: accepted
* Deciders: bvickery, ghendricks, lofiesh, jkemp, mrittman, epentz, jwass, board
* Date: 2022-04-04

Technical Story: Epic [CR-154](https://crossref.atlassian.net/browse/CR-154)

## Context and Problem Statement

Every Crossref member has been able to specify their reference visibility policy: one of open, closed or limited. This flag is associated with the Owner Prefix object. There are important policy implications for this, which are covered in the linked board paper.

Technically, the effect of the policy is that whenever we serve metadata over APIs (REST API, UNIXSD in Content System) we must look up the current owner prefix of the DOI in question

This introduces a coupling between the metadata model and the community data model - we can't serve metadata without consulting the membership model. Apart from the coupling of otherwise unrelated domains, it means that we can only serve metadata in the format of self-contained metadata records (in e.g. UNIXSD and Citeproc-JSON). We can't decompose those into individual metadata assertions (as required to make the Item Graph / Research Nexus come true) 

## Decision Drivers

* POSI target of open metadata
* Open Item Graph
* Reduce coupling in metadata 

## Decision Outcome

All references to be open as of 2022-05-03. Board vote.

### Positive Consequences

* Meets POSI target of open metadata
* Allows us to simplify membership metadata. One field removed from the 'owner prefix' data model.
* Allows us to remove legacy code around reference-visibility
* Unblocks having an open item graph
* Removes 

## Links

* [Blog post](https://www.crossref.org/blog/amendments-to-membership-terms-to-open-reference-distribution-and-include-uk-jurisdiction/)
* [Board paper](https://docs.google.com/document/d/1r7lEmFUDNQANA3d2xJPhAsuui4KLL-eKvIKUZZpZagU/edit)
