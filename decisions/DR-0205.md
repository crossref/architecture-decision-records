---
tags:
  - design
  - metadata
  - item-graph
  - item-tree
  - manifold
---
# DR-0205: Cayenne's Item Tree parser as the basis for the metadata representation in Manifold Item Graph

* Status: accepted
* Deciders: jwass, pfeeney, mrittman, dtkaczyk, gbilder
* Date: 2022-02-22

Technical Story: <https://crossref.atlassian.net/browse/CR-73>

## Abstract

Currently we use documents (i.e. [bibliographic records](https://en.wikipedia.org/wiki/Bibliographic_record)) to represent bibliographic metadata about registered content items. This is convenient for producers and consumers of the metadata in our membership and in the broader scholarly community. The Research Nexus is a new framing of our place in the ecosystem and the way that we handle metadata. It emphasises the connections between scholarly objects, allowing more parties beyond the 'publisher' to make metadata assertions, along with the accurate attribution of provenance for those assertions. This poses a number challenges (described below) to the purely document-based model.

To achieve the Research Nexus framing we need a model that allows metadata assertions to be made by multiple parties in a way that consistently encodes provenance. The model should also allow assertions to be made at any scope (e.g. whole metadata record, focus on a single funding grant, or simply what an author's name is).

The need for a document-based representation has not gone away for either producers or consumers. But the use-cases are different in each case: what's convenient for a publisher to express may not be precisely what another member of the community wants. 

We need a metadata model, and a supporting metamodel, that's a natural fit for both worlds. The Item Tree is that. It's a simple tree-based metamodel that represents Items, Properties and Relationships and it already powers the internals of the REST API. It is used to model our bibliographic metadata, and this model has a proven track record. This translation, from a complex XSD model to this simple general metamodel, is no small achievement and is a highly valuable technical asset. We have also demonstrated, with an advanced proof of concept, that it can be used to build a graph (i.e. the union of all Item Trees in one database) with provenance attribution.

Adopting the item tree model means making some structural and vocabulary adjustments, but they are tweaks, not fundamental changes. Once those are done, it can form the basis for our item graph data model. Note that most of these adjustments constitute technical debt and bugs that we would need to address anyway as part of maintenance of our REST API codebase. Few of them are purely for the benefit of the item graph.

## Context and Problem Statement

Crossref bibliographic metadata has historically been distributed in the form of documents, which describe registered content items such as articles or books. These are encoded in the XML format, and express metadata using the UNIXML model, our custom metadata schema. Distributing metadata in documents is mutually useful to both producers and consumers and has been, for the most part, convenient to both parties. 

A document represents not only the item in question (e.g. article) but also a cluster of related items (e.g. author's name). Any model we adopt must be able to represent those assertions with appropriate intent. For example, when a journal article record says "this article was written by an author with this name and this ORCID ID", there's an implicit assertion that the author has the given name and the given ORCID ID. Whilst the journal publisher may want to assert this in the context of the article, different publishers may disagree, and the author may also want control over what their name is. Some adjustment is needed in the model to record these kinds of information appropriately. [[DR-0415]] explores this question in some depth and explains our reification strategy. 

Side-note: We do allow resource-only deposits, but these are each custom and narrow in scope. They can only be applied to a prior XML deposit, and can't be made by anyone other than the custodian of the metadata (i.e. publisher). And they can't be generalized out to arbitrary assertions of metadata. Similarly, XML patches might help existing members express changes to the document in tighter scope, but they wouldn't allow anyone *else* to make such assertions.

Our present XML metadata schema defines, top down, all possible assertions we allow about the cluster of related items. In the above authorship example, it might be desirable to let an author express an assertion of their own name. However, even if we did allow an author to submit a fragment of the broader XML document, there's no meaningful way to integrate this with the XML documents from publishers. What we need to cover this case is an internal model that has a concept of an author, and a metamodel that lets us model arbitrary objects (e.g. authors, journals, funders, book chapters, organizations, etc) so that assertions can be attached to them. Not only that, but there must be a bridge between the XML document expression and this generalized model. 

### Format

We're interested in two levels of representation: the metamodel (e.g. item tree vs XSD vs JSON) and the model (e.g. a specific item tree vocabulary vs UNIXSD schema vs Citeproc-JSON). We want a solution at both levels. 

Converting between formats (at both levels) enables content negotiation. A data model that's independent of format is useful in this regard. It allows members to express themselves in the most convenient and mutually understood format and vocabulary, but doesn't force users to understand all the details. Instead, they can content-negotiate into a format that's tailored to their use.

The REST API provides a JSON version of the same metadata. Although the format (JSON) and data model (CiteProc) is different, it is derived from the same XML and therefore inherits the same provenance and framing: "this is the metadata as supplied by the publisher of the content". 


On the metamodel level, being able to express assertions at an arbitrary scope is important. Therefore the metamodel needs to be general enough that we can allow assertions at any level. The Item Tree does this by using identifiers attached to Items at all levels of the tree (including synthetic blank node identifiers where required). An assertion at any node of the Item Tree can then be asserted in isolation. 

The XML model wouldn't work well in this case. We embed implicit context in various structures meaning that in many cases we couldn't meaningfully do this. Forcing identifiers, as the Item Tree (and RDF) does, makes this implicit model explicit. 

### Event Data

Event Data models citations and other activity as triples, i.e. relationships. This model is almost identical to a single node of an Item Tree, and differs only in vocabulary. Being able to combine these data sources is highly desirable.

## Decision Drivers 

1. Continue to support document-based ingestion in a format useful to members 
2. Continue to support document-based distribution in a format useful to various segments of our wider community
3. Support graph model and all the use cases (e.g. Relationships API endpoint, reference matching, content negotiation and dozens of others)
4. Doesn't require extensive data remodelling project or code freezes.
5. Reduce technical debt in our codebases, especially around data modelling. Bring convergence to our codebases.
6. Support a consolidation of our data model across currently disparate locations, e.g.: member-supplied metadata, organization metadata, ORCID workflows, ROR workflows, funder registry, Citeproc-JSON
7. Support transactional workflows (e.g. ref matching, notifications) not only simple metadata distribution.
8. Avoid a single unified schema as the single means of information arbitrage between members and the wider community. Enable a data model that can accommodate all kinds of corners and divers metadata representations and vocabularies. 
9. Model objects (e.g. institutions) as they appear in the wild (e.g. using vocabulary the institution wants to express), not according to how one branch (e.g. subscription science publishing) sees them.  
10. Allow Event Data to converge with Member-supplied Metadata.
11. Compliance with Principles of Open Scholarly Infrastructure, especially Open Data and Open Source.

## Considered Options

This decision is complex, but it boils down to "should we take advantage of the Item Tree implementation?". There aren't any better options available, and all other options would require a significant amount of re-writing, data modelling and disruption that we don't have the capacity for. 

1. Expose Content System's internal data model
2. Item Tree Data Model
3. Wikibase metamodel
4. Some other framework like pure RDF, e.g. [Vivo](https://www.lyrasis.org/DCSP/Pages/VIVO.aspx)

## Decision Outcome

Option 1: Item Tree metamodel. This has three levels which will need different treatment.

Level 1: Metamodel of Items, Properties, Relationships. This is relatively unchanged, but every Property and Relationship statement is asserted with an 'asserted-by' property, which is a pointer to an Item. This is how we unify trees into a graph.

Level 2: Data model, i.e. how we encode our XML-expressed data model into Item Tree. This will take a bit of work to review and adjust. There is a certain amount of technical debt buried in the API parser pipeline (XML -> Item Tree -> Citeproc-JSON) which is hidden, but is the cause of bugs that we know we want to fix anyway. No fundamental adjustments need to be made though.

Level 3: Parser implementation. Load Cayenne the parser code (which is written in Clojure) into Manifold (which is written in Kotlin). We have a working proof of concept for this. This allows us to keep making fixes to the REST API which are mutually beneficial to the current REST API functionality and the Item Graph functionality. 

### Positive Consequences 

1. We can bring mutual benefit to the REST API `/works` Citeproc-JSON endpoint, whilst improving the data model in the item graph
2. Codebases converge
3. Flexible data model that can be used to build lots of other functionality we want, especially around workflows and reporting
4. Allows vocabulary mapping which enables broader content negotiation.
5. Gives us a migration path for a broad chunk of model that's currently stuck in the legacy Content System codebase.
6. POSI compliant - can be open source, internal data can all be open
7. Meets the spirit of Research Nexus

### Negative Consequences 

1. Requires some up-front work to review technical debt in the Cayenne codebase. But this will benefit the REST API citeproc-json endpoint.
2. Parser is written in Clojure, which is a little arcane. But we already have to support the REST API, so this isn't a meaningful change.

## Pros and Cons of the Options 

### 1: Expose Content System's internal data model

The legacy Content System codebase has a data model for some, but not all, metadata. However the codebase contains a significant amount of technical debt, including a lack of test coverage. The data model doesn't encode provenance in a uniform way (when it does, it attributes on `<citation>` elements, with no broader vocabulary). The data model is overly specific, being optimised for certain workflows and query patterns, but not in an easy to generalize way. The legacy Content System codebase serves its current purpose, but not the objectives expressed in the Research Nexus framing.

Cons:

1. Large amount of technical debt
2. Unsuited to recording provenance
3. Insufficiently general to be an extensible starting point
4. Unsuitable to open-sourcing without significant effort, so it's not a sound starting place in POSI terms.
5. Would leave the REST API in a strange position as a read-only model.

### 2: Item Tree Data Model

Cayenne is the codebase that powers version 1 of the REST API. It contains an internal abstraction, called the Item Tree. Item Tree is a tree-shaped data structure composed of nodes, which have properties, and relationships between them. See [[DR-0010]] for an illustration of how this helped the REST API migration from SOLR to Elastic search. 

A key property of the Item Tree, and the clue is in the name, is that it is a tree. This means that one can pick a root node and retrieve all properties and relationships associated with it. Applying this recursively, with appropriate filters, it's possible to retrieve approximately the same tree as was submitted. This is generalizable into a graph.

The Item Tree metamodel is a good software engineering abstraction as it allows a recursive immutable style of XML parsing, which Clojure is well suited to. It's also general enough to support ingestion from a wide range of formats, and translation back into a similarly wide range of formats.

Pros:

1. Item Tree metamodel has been shown to work well in the REST API internals.
2. Item Tree data model has been shown to work well in the REST API.
3. Item Tree metamodel easily generalizable to an Item Graph.
4. Item Tree metamodel supports loose coupling to Identifiers, which enables extensibility to other metadata domains (e.g. other DOI registration agencies).
5. Item Tree parser in Clojure allows integration with other JVM language (Kotlin).
6. Metamodel allows two-legged reified relationships to be made with accurate provenance attribution for both legs (explored in [[DR-0415]])

Cons:

1. Requires some up-front work to review technical debt in the Cayenne codebase. But this will benefit the REST API Citeproc-JSON endpoint.

### 3: Wikibase metamodel

We could load data into WikiBase. The metamodel is similar, but has some vital differences.

Pros:

1. Already exists
2. Metamodel is quite close and flexible.
3. Records provenance for statements.
4. Open source, pre-existing




Cons:

1. In tests, ingestion was incredibly slow for our intended use case. Would require orders of magnitude to use in our current pipeline.
2. Dependency on BlazeGraph for most use-cases, which is now unsupported.
3. Open source but WMF don't encourage participation in the codebase from the wider community, meaning we would need to fork the codebase. There was no scheme for routine feature sponsorship. 
4. Workflows designed for human and light bot ingestion, not purely machine ingestion.
5. We would need to negotiate with the community for our vocabulary, or fork the vocabulary.
6. Written in PHP which would be hard to integrate with our valuable JVM code. And would require re-skilling team in a language where this is not a mainstream use.

### 4: Some other framework like pure RDF, e.g. [Vivo](https://www.lyrasis.org/DCSP/Pages/VIVO.aspx)

This is the wildcard option and would require a significant rebuild. We do expose RDF in our REST API content negotiation but it's a small special-purpose subset of the overall data model. It is not generalized.

Traditional approaches to retrieval of data from knowledge graph include graph query languages like SPARQL and [Gremlin](https://en.wikipedia.org/wiki/Gremlin_(query_language)). Whilst these are useful for answering specific kinds of queries, at the time of writing the overriding use case for both expressing and retrieving metadata, is document based. So we would need to write parsers from XML to RDF, but also from RDF to Citeproc-JSON. This would be a significant undertaking.

We can certainly use RDF as an ingestion and output format. But as an internal storage format it wouldn't be suitable.

Pros:

1. Participate in wider Knowledge Graph / Linked Open Data ecosystem.
2. Use tried and tested metamodel and data model (e.g. RDF, RDF-Schema, OWL, SKOS)

Cons:

1. We would need to buy in 100% to RDF for it to be a success. RDF Schema is somewhat unwieldy, and a transition that meets the objective of facilitating workflows (not only metadata distribution) would require a significant re-write and re-design of the schema. 
2. RDF doesn't adequately model provenance, see [[DR-0415]] for further discussion.
2. Unlike Item Tree, no existing codebase and parser. Would require an extra translation layer which would add more maintenance cost. 

## Links 

* [[DR-0415]]
* [[DR-0010]]