To add another record run `new.sh`, it will create a new sequentially numbered file.

Run `index.sh` to regenerate the README.md index. This should be committed.

Uses [this](https://github.com/joelparkerhenderson/architecture-decision-record/blob/main/templates/decision-record-template-madr/index.md) template.

Recommended to use Obsidian and [File Explorer Markdown Titles](https://github.com/Dyldog/file-explorer-markdown-titles) to edit.

