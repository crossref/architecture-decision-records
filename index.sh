#!/bin/bash
set -e

INDEX="decisions/index.md"
DECISIONS_DIR="decisions"

# Replace file.
cp etc/INDEX_header.md $INDEX

# List of each file in order.
COUNT=0
for f in "$DECISIONS_DIR"/DR-*.md; do
  echo "Index $f"
  # Take first header line (title) and remove leading '# '.
  title=$(cat $f | grep "#" | head -n 1)
  title=${title/[#] /}
  #file_base=${f/.md/}
  file_base=$(basename $f)
  COUNT=$(( COUNT + 1 ))
  echo "- [$title]($file_base)" >> $INDEX
done
echo "\n$COUNT records" >> $INDEX
cat etc/INDEX_footer.md >> $INDEX
